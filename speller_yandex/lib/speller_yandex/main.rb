module SpellerYandex
  class Main

    def self.parse text, wrong_words
      array = JSON.parse  wrong_words
      array.each do |hash|
        word = hash["word"]
        s = hash["s"].join
        text = text.gsub(word, s)
      end
    text
    end
  end
end
