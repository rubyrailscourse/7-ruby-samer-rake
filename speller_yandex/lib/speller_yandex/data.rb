module SpellerYandex
  module Data

    DATA_URL = "http://speller.yandex.net/services/spellservice.json/checkText?text="

    def self.import text
      data_url_acii = URI.encode(DATA_URL + text)
      wrong_words = open data_url_acii
      wrong_words.read
    end
  end
end
