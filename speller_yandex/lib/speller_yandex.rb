require 'json'
require 'open-uri'
require_relative 'speller_yandex/main'
require_relative 'speller_yandex/data'

module SpellerYandex

  def self.check text
    wrong_words = Data.import text
    Main.parse text, wrong_words
  end
end
