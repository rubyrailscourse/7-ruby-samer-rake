Gem::Specification.new do |spec|
  spec.name          = 'speller_yandex'
  spec.version       = '1.0.4'
  spec.executables   << 'speller_yandex'
  spec.date          = '2016-02-16'
  spec.summary       = 'Text correction'
  spec.description   = 'This gem helps you to correct errors in the text'
  spec.authors       = [ "Alexey Spiridonov" ]
  spec.email         = 'alex9spiridonov@gmail.com'
  spec.homepage      = 'http://rubygems.org/gems/speller_yandex'
  spec.license       = 'MIT'

  spec.files         = [
    "lib/speller_yandex/data.rb",
    "lib/speller_yandex/main.rb",
    "lib/speller_yandex.rb"
  ]

  spec.requirements = %w{
    Internet\ connection,
    A\ good\ mood
  }
end
