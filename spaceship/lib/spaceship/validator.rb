module Spaceship
  module Validator
    class JumpError < StandardError; end

    ENERGY_MIN = 300

    def validate_before_jump
      raise JumpError, "Сoordinates not specified" if @coordinates.empty?
      raise JumpError, "Protective field isn't included" unless @protective_field
      raise JumpError, "Not enough energy" if @energy < ENERGY_MIN
    end

    def validate_after_jump
      raise JumpError, "Unsuccessful jump. Something went wrong!" unless @current_position == @coordinates
    end
  end
end
