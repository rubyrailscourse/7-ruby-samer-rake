require_relative 'validator'

module Spaceship
  class Ship
    include ActiveSupport::Callbacks
    include Validator

    define_callbacks :validate
    set_callback :validate, :before, :validate_before_jump
    set_callback :validate, :after, :validate_after_jump

    attr_accessor :protective_field, :coordinates, :energy, :current_position, :error

    def initialize
      @energy = 0
      @coordinates = []
      @protective_field = false
      @current_position = random_position
    end

    def fill
      @energy = 800
    end

    def switch_protective_field
      @protective_field = !@protective_field
    end

    def random_position
      right_ascension = rand(0..360)
      declension = rand(-90..90)
      [right_ascension, declension]
    end

    def set_coordinates(right_ascension, declension)
      @coordinates = [right_ascension, declension]
    end

    def to_s
      puts "Energy: [#{@energy}], current position: #{@current_position}, coordinates: #{@coordinates}, protective_field: [#{@protective_field}]"
    end

    def jump!
      run_callbacks :validate do
        @current_position = @coordinates
        puts "Successful jump! Current position: #{@coordinates}"
      end
    rescue JumpError => error
      puts error.message
    end
  end
end
