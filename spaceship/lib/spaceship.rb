require 'active_support/callbacks'
require_relative 'spaceship/ship'
module Spaceship
  def self.create
    Ship.new
  end
end

orion = Spaceship.create
orion.jump!
orion.set_coordinates(330, -43)
orion.jump!
orion.switch_protective_field
puts orion
orion.jump!
orion.fill
orion.jump!
